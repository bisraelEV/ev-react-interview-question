import "./App.css";
import React, { useEffect, useState } from "react";
import { Forecast } from "./types/Types";
import { format } from "date-fns";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

const card = (
  <React.Fragment>
    <CardContent>
      <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
        Energy Vault React Interview Question
      </Typography>
      <Typography variant="h5" component="div">
        Windy Weather UI
      </Typography>
      <Typography align="justify" variant="body2">
        Energy Management Systems rely very heavily on weather forecasting for
        planning and optimization. We would like for you to create a user
        interface to render weather forecasts for an employee at a Wind Turbine
        facility in Tyson's Corner, Virginia. The purpose of the UI is to
        provide the user a solid sense of the changes in weather over the course
        of a day. The user is concerned about the direction and speed of the
        wind so they can get an idea of how and how well a wind turbine may
        work. They are also concerned other aspects of weather such as
        temperature and temperature changes as that may affect equipment and
        people on the site.
        <br />
      </Typography>
    </CardContent>
    <CardActions>
      <Button
        size="small"
        href="https://www.weather.gov/documentation/services-web-api"
      >
        National Weather Service API Documentation
      </Button>
    </CardActions>
  </React.Fragment>
);

const App = () => {
  return (
    <Box className={"root"}>
      <div className={"container"}>
        <Card className={"card"} variant="outlined">
          {card}
        </Card>
      </div>
    </Box>
  );
}

export default App;